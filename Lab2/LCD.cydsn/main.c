/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
//    static uint16* CNT; 
//    uint16 start_CNT = 50;
//    CNT = &start_CNT;
//    LCD_Start();
//    LCD_PrintString("Count");
//    LCD_Position(1,0);
//    LCD_PrintNumber(*CNT);
//    LCD_Position(1,8);
//    LCD_PrintInt16(*CNT);
//    for(;;)
//    {
//        /* Place your application code here. */
//        if (SW1_Read() == 0){
//        CyDelay(5);
//        while(SW1_Read() == 1)
//        {
//            CyDelay(100);
//            *CNT= *CNT +1;
//            if (*CNT >= 100)
//            {
//                    *CNT = 100;
//            }
//            LCD_ClearDisplay();
//            LCD_Position(0,0);
//            LCD_PrintString("Count Up");
//            LCD_Position(1,0);
//            LCD_PrintNumber(*CNT);
//            LCD_Position(1,8);
//            LCD_PrintInt16(*CNT);
//        }
//        }
//        while(SW2_Read() == 0);
//        CyDelay(50);
//        while(SW2_Read() == 1)
//        {
//            CyDelay(100);
//             *CNT = *CNT -1;
//             if (*CNT >= 100)
//                {
//                    *CNT = 0;
//                }
//            LCD_ClearDisplay();
//           LCD_Position(0,0);
//           LCD_PrintString("Count Down"); 
//           LCD_Position(1,0);
//           LCD_PrintNumber(*CNT);
//           LCD_Position(1,8);
//           LCD_PrintInt16(*CNT);
//        }
//    }
    
    
    
    uint16 CNT;
    uint16 start_CNT = 50;
    CNT = start_CNT;
    LCD_Start();
    LCD_PrintString("Count");
    LCD_Position(1,0);
    LCD_PrintNumber(CNT);
    LCD_Position(1,8);
    LCD_PrintInt16(CNT);
    for(;;)
    {
        /* Place your application code here. */

        if(SW1_Read() == 1){
            CyDelay(5);
            if(SW1_Read() == 1){
                CNT = CNT + 1;
                if (CNT >= 100){
                    CNT = 100;
                }
                LCD_ClearDisplay();
                LCD_Position(0,0);
                LCD_PrintString("Count Up");
                LCD_Position(1,0);
                LCD_PrintNumber(CNT);
                LCD_Position(1,8);
                LCD_PrintInt16(CNT);
            }
            while(SW1_Read() == 1);
        }
        if(SW2_Read() == 1){
            CyDelay(5);
            if(SW2_Read() == 1){
                CNT = CNT - 1;
                if (CNT <= 0){
                    CNT = 0;
                }
                LCD_ClearDisplay();
                LCD_Position(0,0);
                LCD_PrintString("Count Down");
                LCD_Position(1,0);
                LCD_PrintNumber(CNT);
                LCD_Position(1,8);
                LCD_PrintInt16(CNT);
            }
            while(SW2_Read() == 1);
        }
    }   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

/* [] END OF FILE */
